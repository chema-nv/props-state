import React,{useState} from 'react';// importar para usar variable de estado
import { CounterViewer,CounterButtons} from './componets';
import './App.scss';


function App() {
  //la veriable de estado se actualiza y renderiza automaticamente a detectar el cambio
  //
  const [counter,setCounter]=useState(0);//inicializamos a 0 la var de estado setCounter2
      //counter variable de estado
      //setCounter funcion que modifica la variable de stado
  

  const sumar = () => {
 
    setCounter(counter+1);//cambiamos veriable de estado
  };
  const restar = () => {
    if (counter>=1) {
      setCounter(counter-1);//cambiamos variable de estado
    }
    
  }



  return (
    <div className="App">

      <CounterViewer counter={counter}/> 
      {/* Al pasarle counter "variable de estado" como props, re-renderiza aunque sea en el compnete
       */}

      <h1 className={counter<=10? 'rojo': 'azul'}>CONTADOR DE APP: {counter}</h1>

      <CounterButtons sumar={sumar} restar={restar}/>
      {/* al componete counterButtons le pasamos por props las funciones suma y resta */}

      {/* <button className='button' onClick={sumar}>+</button>
      <button className='button' onClick={restar}>-</button> */}

    </div>
  );
}

export default App;
