const CounterButtons = ({sumar,restar}) =>{
    //destructuramos las props para usarlas en los botones
    return (
        <div>
            <button className='button' onClick={sumar}>+</button>
            <button className='button' onClick={restar}>-</button>
        </div>
    )
}

export default CounterButtons;